# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippets', '0002_auto_20150907_0154'),
    ]

    operations = [
        migrations.RenameField(
            model_name='snippet',
            old_name='highlighted',
            new_name='highlihted',
        ),
    ]

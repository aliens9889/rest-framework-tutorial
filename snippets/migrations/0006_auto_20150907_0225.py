# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippets', '0005_auto_20150907_0210'),
    ]

    operations = [
        migrations.RenameField(
            model_name='snippet',
            old_name='highlihted',
            new_name='highlighted',
        ),
    ]

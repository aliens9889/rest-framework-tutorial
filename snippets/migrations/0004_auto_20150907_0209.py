# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('snippets', '0003_auto_20150907_0206'),
    ]

    operations = [
        migrations.AlterField(
            model_name='snippet',
            name='code',
            field=models.TextField(null=True),
        ),
        migrations.AlterField(
            model_name='snippet',
            name='highlihted',
            field=models.TextField(null=True),
        ),
    ]
